
var box = document.querySelector('#box');
var button = document.querySelector('#button');
var title = document.querySelector('#title');
var numberOfStudents = 0;

button.addEventListener("click", function(){

	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", ajaxLoad);
	xhr.addEventListener("error", ajaxError);
	xhr.open("GET", "students.json");
	xhr.send();

	function ajaxLoad(event) {

		if (this.status === 200) {

			var text = this.responseText;
			var student = JSON.parse(text);
			var keys = Object.keys(student);

			keys.forEach(function(key) {
				student[key].forEach(function(item) {

					numberOfStudents += 1;

					// Variabelen assignen aan objecten uit JSON
					var name = item.Name;
					var age  = item.Age;
					console.log(name +" is "+age+ " jaar oud.");
					button.disabled = true;

					var trRowStudent = document.createElement("TR");

					// kolom voor naam
					var tdColumnName = document.createElement("TD"); 
					var nameColumn = document.createTextNode(name); 
					tdColumnName.appendChild(nameColumn);
					document.body.appendChild(tdColumnName);

					// kolom voor leeftijd
					var tdColumnAge = document.createElement("TD"); 
					var ageColumn = document.createTextNode(age); 
					tdColumnAge.appendChild(ageColumn);

					// Kolommen appenden aan row
					trRowStudent.appendChild(tdColumnName);
					trRowStudent.appendChild(tdColumnAge);

					// row appenden aan body
					document.body.appendChild(trRowStudent);

					button.innerHTML = "JSON gegevens zijn opgehaald!";
					title.innerHTML = numberOfStudents+" studenten gevonden";

				})				
			});
		}

		else {

			console.log("Request niet geluktttt");
		}
	}

	function ajaxError(event) {
		console.log('Fout: AJAX');
	}

});


